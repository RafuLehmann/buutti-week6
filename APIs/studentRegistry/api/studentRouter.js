import { Router } from 'express';
import { writeDB, readDB } from '../database/connectDB.js';

const router = Router();


router.get('/:id', async (req, res) => {
    const students = await readDB();
    const student = students.find((student) => {
        return student.id === Number(req.params.id)
    })
    res.send(JSON.stringify(student))
})

router.get('/', async (req, res) => {
    const students = await readDB();
    console.log("#", JSON.stringify(students))
    res.send(JSON.stringify(students, null, 4))
})

router.post('/', async (req, res) => {
    
    const students = await readDB()
    if (req.body.name && req.body.email && req.body.address) {
        let student = {
            "name": req.body.name,
            "id": students[0].id + 1,
            "email": req.body.email,
            "address": req.body.address
        }
        console.log(students)
        students.unshift(student)
        writeDB(students);
        res.send(`Student ${student.name} is added.`)
            
    } else {
        console.log("error: invalid request");
        res.send("error: invalid request");
    }
})

router.put('/:id', async (req, res) => {
    const students = await readDB()
    const studentIndex = students.findIndex((student) => {
        return student.id === Number(req.params.id)
    })
    if (req.body.name || req.body.email || req.body.address) {
        students[studentIndex].name = req.body.name || students[studentIndex].name;
        students[studentIndex].email = req.body.email || students[studentIndex].email;
        students[studentIndex].email = req.body.address || students[studentIndex].address;
        writeDB(students);
        res.send(`Student ${students[studentIndex].name} is edited.`)
            
    } else {
        console.log("error: invalid request");
        res.send("error: invalid request");
    }
})

router.delete('/:id', async (req, res) => {
    const students = await readDB()
    const studentIndex = students.findIndex((student) => {
        return student.id === Number(req.params.id)
    })
    if (req.body.name || req.body.email || req.body.address) {
        students.splice(studentIndex, 1)
        writeDB(students);
        res.send(`Student ${students[studentIndex].name} is deleted.`)
            
    } else {
        console.log("error: invalid request");
        res.send("error: invalid request");
    }
})

export default router;