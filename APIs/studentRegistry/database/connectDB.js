import fs from 'fs';
import util from 'util';

const readFile = util.promisify(fs.readFile);
const filePath = "./database/students.json"

export const readDB = async () => {
    const value = await readFile(filePath, "utf8");
    try { return JSON.parse(value) }
    catch (err) { console.log("Error reading from db:\n", err)}
} 

export const writeDB = async (dataToSave) => {
    const data = JSON.stringify(dataToSave, null, 4);
    fs.writeFile(filePath, data, "utf8", (err) => {
        if (err) console.log("Error writing into db: \n", err)
    })
} 