import express from 'express';
import studentRouter from './api/studentRouter.js';

const app =  express();
const PORT = 3000;
app.use(express.json())
app.use('/student', studentRouter)


app.listen(PORT, () => {
    console.log(`listening to ${PORT}`)
});