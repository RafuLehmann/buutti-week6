import express from "express";
import helmet from "helmet";
import apiRouter from "./router/apiRouter.js"
import frontEndRouter from "./router/frontEndRouter.js"


const app = express();
const PORT = 3000;
app.use(express.json());
app.use(helmet());
app.disable('x-powered-by');

// for frontend pages
app.use('/', frontEndRouter)
// for apicalls
app.use('/api', apiRouter);

//block access to all other pages
const unknownEndpoint = (_req, res) => {
    res.status(404).send("Access denied. Use /api/.");
}
app.use('*', unknownEndpoint);

// handling errors
const errorHandler = (error, req, res, next) => {
    //logger.error(error);
    if (error) {
        
        res.status(400).send(error.name, "\n", error.message);
    }
    next(error)
}
app.use(errorHandler);

const environment = process.env.NODE_ENV;


environment !== 'test' && app.listen(PORT, ()=> {
    console.log(`Listening to ${PORT}.`);
})

export default app;