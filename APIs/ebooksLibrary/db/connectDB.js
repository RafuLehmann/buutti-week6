import fs from 'fs';
import util from 'util';
const filePath = "./logger/logs.txt"
const readFile = util.promisify(fs.readFile)

export const readDB = async () => {
    const data  = await readFile(filePath, "utf8");
    try { return JSON.parse(data); } 
    catch (err) { console.log(err); }
}

export const writeDB = (dataToSave) => {
    const data = JSON.stringify(dataToSave, null, 4,);
    fs.writeFile(filePath, data, "utf8", (err) => {
        if (err) console.log(err)
    })
}

export const appendFile = (dataToSave) => {
    fs.appendFile(filePath, dataToSave + "\n", (err) => {
        if (err) console.log(err)
    })
}