import { Router } from "express";
import express from "express";
import { fileURLToPath } from "url";

const router = Router();
// const __dirname = new URL('../', import.meta.url).pathname.slice(1);
const __dirname = fileURLToPath(new URL('../', import.meta.url));
const staticFiles = __dirname + "static"
//console.log(staticFiles)
router.use(express.static(staticFiles))
router.get('', (req, res) => {
    res.sendFile(staticFiles + '/index.html');
})
router.get('/add', (req, res) => {
    res.sendFile(staticFiles + '/add.html');
})
router.get('/remove', (req, res) => {
    res.sendFile(staticFiles + '/remove.html');
})

export default router;