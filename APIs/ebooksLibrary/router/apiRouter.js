import { Router } from "express";
import crypto from "crypto";
//import { readDB, writeDB, appendFile } from "../db/connectDB.js";
import logger from "../logger/logger.js"

const router = Router();
const books = [];
router.use(logger);

// list all books
router.get("/", (req, res) => {
    res.type('json')
    res.status(200).json(books)
})

// show requested book
router.get("/:id", (req, res) => {
    const book = books.find(book => book.id === req.params.id);
    if (!book) res.json("Not Found, please check that you have spelled the id correctly!")
    res.send(book)
})

// insert a new book to library
router.post("/", (req, res) => {
    if (req.body.name && req.body.author) {
        const book = {
            "id": crypto.randomUUID(), 
            "name": req.body.name, 
            "author": req.body.author, 
            "read": false
        };
        books.unshift(book);
        res.status(201).json(book)
    } else {
        //console.log("invalid input")
        res.json("Invalid input, add following details: \n{ id: number, name: string, author: string }")
    }
})

// edit details of a book
router.put("/:id", (req, res) => {
    const bookIndex = books.findIndex(book => book.id === req.params.id);
    const prevdetails = books[bookIndex];


    books[bookIndex].name = req.body.name || books[bookIndex].name, 
    books[bookIndex].author = req.body.author || books[bookIndex].author
    books[bookIndex].read = req.body.read || books[bookIndex].read

    
    res.status(201).json(`New details: ${JSON.stringify(books[bookIndex])}.`)
})

// edit details of a book
router.delete("/:id", (req, res) => {
    const bookIndex = books.findIndex(book => book.id === req.params.id);
    const deletedBook = books[bookIndex]
    books.splice(bookIndex, 1)
    res.send(`${deletedBook.name} was successfully deleted.`)
})
const unknownEndpoint = (_req, res) => {
    res.status(404).json("Unknown id.");
}
router.use('*', unknownEndpoint);
export default router;