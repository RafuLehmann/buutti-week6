import app from "../index.js";
import request from "supertest";

describe('Test root, "/":', () => {
    it("Return 200", async () => {
        const res = await request(app)
            .get('/')

        expect(res.statusCode).toBe(200)
    })
    it("Return 404 if a parameter is added", async () => {
        const res = await request(app)
            .get('/:r')

        expect(res.statusCode).toBe(404)
    })
})

/**
 *  Testing API calls 
 **/
let created_id = "";
// testing POST
describe("POST '/api'", (done) => {
    it("--- CHECKING BODY CONTENT ---", async () => {
        const res = await request(app)
            .post('/api/')
            .send({
                "name": "Horry Patter",
                "author": "-"
              })

        expect(res.statusCode).toBe(201)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")
        expect((res) => {
            if (!res.body.hasOwnProperty('id')) throw new Error("expected 'id' key!");
            if (!res.body.hasOwnProperty('name')) throw new Error("expected 'name' key!");
            if (!res.body.hasOwnProperty('author')) throw new Error("expected 'author' key!");
        })
        created_id = res.body['id']
    })
    it("--- INVALID INPUT ---", async () => {
        const res = await request(app)
            .post('/api/')
            .send({
                "name": "Harry Potter",
                "authr": "-"
              })

        expect(res.statusCode).toBe(200)
        expect(res.body).toBe("Invalid input, add following details: \n{ id: number, name: string, author: string }")
    })

})


// testing GET
describe("GET '/api'", (done) => {
    it("--- GET ALL BOOKS ---", async () => {
        const res = await request(app)
            .get('/api/')
            .expect(200)
            
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")
           
    })
    it("--- GET HARRY POTTER BOOK ---", async () => {
        const res = await request(app)
            .get(`/api/${created_id}`)

        expect(res.statusCode).toBe(200)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")         
    })
    it("--- INVALID ID ---", async () => {
        const res = await request(app)
            .get('/api/123')

        expect(res.statusCode).toBe(200)
        expect(res.body).toBe("Not Found, please check that you have spelled the id correctly!")
    })
})


// testing PUT
describe("PUT '/api'", (done) => {
    it("--- CHANGE NAME OF THE AUTHOR ---", async () => {
        const res = await request(app)
            .put(`/api/${created_id}`)
            .send({
                "author": "J. K. Rowling"
              })

        expect(res.statusCode).toBe(201)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")   
        expect(res.body).toBe(`New details: {"id":"${created_id}","name":"Horry Patter","author":"J. K. Rowling","read":false}.`)
    })
    it("--- CHANGE NAME OF THE BOOK ---", async () => {
        const newName = "Harry Potter"
        const res = await request(app)
            .put(`/api/${created_id}`)
            .send({
                "name": `${newName}`
              })

        expect(res.statusCode).toBe(201)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")   
        expect(res.body).toBe(`New details: {"id":"${created_id}","name":"${newName}","author":"J. K. Rowling","read":false}.`)           
    })
    it("--- CHANGE READ STATUS TO TRUE ---", async () => {
        const newStatus = true
        const res = await request(app)
            .put(`/api/${created_id}`)
            .send({
                "read": `${newStatus}`
              })

        expect(res.statusCode).toBe(201)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")   
        expect(res.body).toBe(`New details: {"id":"${created_id}","name":"Harry Potter","author":"J. K. Rowling","read":"${newStatus}"}.`)  
    })
    it("--- INVALID ID ---", async () => {
        const res = await request(app)
            .post('/api/123')
            .send({
                "name": `newBook`
              })

        expect(res.statusCode).toBe(404)
        expect(res.headers["content-type"]).toBe("application/json; charset=utf-8")   
        expect(res.body).toBe("Unknown id.")
    })
})


'New details:\n' + '{"id":"43d96fb3-7e34-48fe-a7a8-bf5946ee12db","name":"Horry Patter","author":"J. K. Rowling","read":false}.'
'New details:\n' + '{"id":"fb955673-9bc2-496b-8ce0-6b7dabc17d4b","name":"Horry Patter","author":"J. K. Rowling","read":false}.'