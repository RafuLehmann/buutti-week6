import { readDB, writeDB, appendFile } from "../db/connectDB.js";


const logToJSON = async (log) =>{
    const database = await readDB();
    database.unshift(log);
    writeDB(database);
}

const logToTXT = (log) =>{
    const txtLog = `[${log.dateTime}] ${log.method}: ${log.url} ${log.status} params:${JSON.stringify(log.params)} body:${JSON.stringify(log.body)}`;
    appendFile(txtLog);
}

const logger = async (err, req, res, next) => {
    const datetime_now = new Date();
    const log = {
        method:  req.method,
        dateTime:  datetime_now.toLocaleString(),
        url: req.url,
        status: res.statusCode,
        params: req.params,
        body: req.body
    }

    logToTXT(log);
    // logToJSON(log); <--- TODO: Fix issue with await
    next();
}

export default logger;