import express from 'express';
import requestApiRouter from "./api/requestApiRouter.js";


const app = express();
const PORT = 3000;
app.use(express.json())
app.use("/", requestApiRouter);



app.listen(PORT, () => {
    console.log(`Listening to ${PORT}.`)
})

