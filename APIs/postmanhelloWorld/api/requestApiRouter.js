import { Router } from "express";

const router = Router();

const animals = [];
router.post('/request', (req, res) => {
    animals.push(req.body)
    console.log(animals)
    res.status(201).send(`Following details added: ${JSON.stringify(animals, null, 4)}`)
})

export default router;